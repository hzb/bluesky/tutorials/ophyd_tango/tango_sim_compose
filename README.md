# ophyd-tango-demo


## jupyter env
for easy access we added a jupyter lab to the accompanying `docker-compose.yaml`
this should work without the need create any environment on the host system.

just enter into the jypyter lab with

```
http://localhost:8888/lab?token=myToken
```


## Getting started / preparation of the environment

Starting point of what is described is an empty mamba/conda environment

- prepare the mamba/conda env
```
mamba install --file requirements.txt -c conda-forge
```

- fetch the git submodule `ophyd-async` https://github.com/desy-fsec/ophyd-async.git 
- install ophyd+bluesky

```
cd ophyd-async/
git submodule init
git submodule update
pip install -e .
cd ..
```

- run `docker-compose` to start the tango db and device servers

```
docker-compose up -d
```

## run the first demo script

based on `ophyd-async/docs/user/examples/tango_scan.py`. a modified version 
of this file is in the root of this repo that uses the tango path available in 
the docker container.

```
TANGO_HOST=localhost:10000 python tango_scan.py
```

Note 1: make sure that the environment variable `TANGO_HOST` is set so that the python process can connect to the tango db inside the docker container
Note 2: it may take a few seconds before the scan starts. ... for the first scan it may even take almost a minute before the scan actually runs.

## in case of trouble with tango servers not starting
try to flush the mariadb and start servers again after having used
```
docker-compose down -v
```


## Resources
- the forked ophyd-async repo with tango support: https://github.com/desy-fsec/ophyd-async.git 
- docker container + docker compose to run a tango DB + servers (preconfigured like at DESY): 
  https://gitlab.desy.de/marcus.fleck/fsecservers-in-default-tango-cs-docker



## to inspect Tango 
  in order to inspect the tango DB the most common tool to use would be `jive`. If conda is at hand, it 
  can e.g. installed using the conda env file provided in `tango_tool_env/tango_tools.yml` with 
  ```
  conda env create -n tango_tools --file tango_tool_env/tango_tools.yml
  conda activate tango_tools
  TANGO_HOST=localhost:10000 jive
  ```
